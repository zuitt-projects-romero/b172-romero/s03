
-- Create a database.
CREATE DATABASE blog_db;

-- Select a database.
USE blog_db;

-- Create tables
-- Table columns have the following format: [column_name] [data_type] [other_options]
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(500),
    content VARCHAR(5000),
    datetime_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);



CREATE TABLE post_likes (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_likes_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_users_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


CREATE TABLE post_comments(

    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000),
    datetime_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_post_id
        FOREIGN KEY (post_id) REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);



-- Insert values in albums table
INSERT INTO users(email, password) VALUES ("johnsmith@gmail.com", "passwordA");

INSERT INTO users(email, password) VALUES ("juandelacruz@gmail.com", "passwordB");

INSERT INTO users(email, password) VALUES ("janesmith@gmail.com", "passwordC");

INSERT INTO users(email, password) VALUES ("mariadelacruz@gmail.com", "passwordD");

INSERT INTO users(email, password) VALUES ("johndoe@gmail.com", "passwordE");

-- Inserting data to posts 
INSERT INTO posts(user_id, title, content) VALUES (1, "First Code", "Hello World!");

INSERT INTO posts(user_id, title, content) VALUES (1, "Second Code", "Hello Earth!");

INSERT INTO posts(user_id, title, content) VALUES (2, "Third Code", "Welcome to Mars!");

INSERT INTO posts(user_id, title, content) VALUES (4, "Fourth Code", "Bye bye solar system!");


-- Selecting Records

SELECT * FROM users WHERE id = 1

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

DELETE FROM users WHERE email = "johndoe@gmail.com";
